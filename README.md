This repository contains CRUD REST API server for our day 18 assignment in Glints Academy #13

Members:

```
cimbraien
ffuad13
PandjiBayu
gemadar
```

---

HOW TO TEST

```bash
# Firstly, create a database in your mysql server, name it anything (DBNAME)
# Import database
mysql -u USERNAME -p DBNAME < ./test/sales.sql
```

We have included the postman collection that contains all the request needed to test the api server in directory ./test
