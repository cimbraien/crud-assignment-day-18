const { query } = require("../models");

class Customers {
  async getAllCustomers(req, res) {
    try {
      const dataCustomers = await query(`SELECT * FROM customers`);
      res.status(200).json({
        dataCustomers,
      });
    } catch (error) {
      res.status(500).json({
        message: error.message,
      });
    }
  }

  async getCustomerById(req, res) {
    try {
      const dataCustomers = await query(
        `SELECT * FROM customers WHERE id = ${req.params.id}`
      );
      if (dataCustomers.length == 0) {
        return res.status(404).json({
          message: `Id = ${req.params.id} is not found in Customer`,
        });
      }
      res.status(200).json({
        dataCustomers,
      });
    } catch (error) {
      res.status(500).json({
        message: error.message,
      });
    }
  }

  async createCustomer(req, res) {
    try {
      const insertCustomer = await query(
        `INSERT INTO customers (name) VALUES ("${req.body.name}")`
      );
      const dataCustomers = await query(
        `SELECT * FROM customers WHERE id = ${insertCustomer.insertId}`
      );
      res.status(201).json({
        dataCustomers,
      });
    } catch (error) {
      res.status(500).json({
        message: error.message,
      });
    }
  }

  async updateCustomer(req, res) {
    try {
      const customer = await query(
        `SELECT * FROM customers WHERE id = ${req.params.id}`
      );
      if (customer.length === 0) {
        return res.status(404).json({
          message: `Id = ${req.params.id} is not found in Customer`,
        });
      }
      await query(
        `UPDATE customers SET name="${req.body.name}" WHERE id = ${req.params.id};`
      );
      const dataCustomers = await query(
        `SELECT * FROM customers WHERE id = ${req.params.id}`
      );
      res.status(201).json({
        dataCustomers,
      });
    } catch (error) {
      res.status(500).json({
        message: error.message,
      });
    }
  }

  async deleteCustomer(req, res) {
    try {
      const delCustomer = await query(
        `DELETE FROM customers WHERE id=${req.params.id}`
      );

      if (delCustomer.affectedRows === 0) {
        return res.status(404).json({
          message: "Customer not found",
        });
      }

      res.status(200).json({
        message: `Customer with id ${req.params.id} has been deleted`,
      });
    } catch (error) {
      res.status(500).json({
        message: error.message,
      });
    }
  }
}

module.exports = new Customers();
