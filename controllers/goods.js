const { query } = require("../models");

class Goods {
	async addGoods(req, res) {
		try {
			const addGood = await query(
				`INSERT INTO goods (name, price, id_supplier) VALUES ("${req.body.name}", ${req.body.price}, ${req.body.id_supplier})`
			);

			const data = await query(
				`SELECT g.id, g.name as goods, s.name as supplier, g.price as price FROM goods g JOIN suppliers s ON s.id = g.id_supplier WHERE g.id=${addGood.insertId}`
			);

			res.status(201).json({
				data,
			});
		} catch (error) {
			res.status(500).json({
				message: error.message,
			});
		}
	}

	async getAllGoods(req, res) {
		try {
			const data = await query(
				"SELECT g.id, g.name as goods, s.name as supplier, g.price as price FROM goods g JOIN suppliers s ON s.id = g.id_supplier"
			);

			res.status(200).json({
				data,
			});
		} catch (error) {
			res.status(500).json({
				message: error.message,
			});
		}
	}

	async getOneGoods(req, res) {
		try {
			const data = await query(
				`SELECT g.id, g.name as goods, s.name as supplier, g.price as price FROM goods g JOIN suppliers s ON s.id = g.id_supplier WHERE g.id=${req.params.id}`
			);

			if (data.length === 0) {
				return res.status(404).json({
					message: "Goods is not found",
				});
			}

			res.status(200).json({
				data,
			});
		} catch (error) {
			res.status(500).json({
				message: error.message,
			});
		}
	}

	async editGoods(req, res) {
		try {
			const good = await query(`SELECT * FROM goods WHERE id=${req.params.id}`);

			if (good.length === 0) {
				return res.status(404).json({
					message: "Transaction not found",
				});
			}

			await query(
				`UPDATE goods SET name="${req.body.name}", price=${req.body.price}, id_supplier=${req.body.id_supplier} WHERE id=${req.params.id}`
			);

			const data = await query(
				`SELECT g.id, g.name as goods, s.name as supplier, g.price as price FROM goods g JOIN suppliers s ON s.id = g.id_supplier WHERE g.id=${req.params.id}`
			);

			res.status(201).json({
				data,
			});
		} catch (error) {
			res.status(500).json({
				message: error.message,
			});
		}
	}

	async deleteGood(req, res) {
		try {
			const deleted = await query(
				`DELETE FROM goods WHERE id=${req.params.id}`
			);

			if (deleted.affectedRows === 0) {
				return res.status(404).json({
					message: "Good not found",
				});
			}

			res.status(200).json({
				message: `Good with id ${req.params.id} has been deleted`,
			});
		} catch (error) {
			res.status(500).json({
				message: error.message,
			});
		}
	}
}

module.exports = new Goods();
