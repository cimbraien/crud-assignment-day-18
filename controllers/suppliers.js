const { query } = require("../models");

class Suppliers {
	async getAllSuppliers(req, res) {
		try {
			const data = await query(`SELECT * FROM suppliers`);
			res.status(200).json({
				data: data,
			});
		} catch (error) {
			res.status(500).json({
				message: error.message,
			});
		}
	}

	async getSupplierById(req, res) {
		try {
			const data = await query(
				`SELECT * FROM suppliers WHERE id = ${req.params.id}`
			);
			if (data.length == 0) {
				return res.status(404).json({
					message: `Supplier with id = ${req.params.id} is not found`,
				});
			}
			res.status(200).json({
				data: data,
			});
		} catch (error) {
			res.status(500).json({
				message: error.message,
			});
		}
	}

	async createSupplier(req, res) {
		try {
			const data = await query(
				`INSERT INTO suppliers (name) VALUES ("${req.body.name}")`
			);
			const inserted = await query(
				`SELECT * FROM suppliers WHERE id = ${data.insertId}`
			);
			res.status(201).json({
				data: inserted,
			});
		} catch (error) {
			res.status(500).json({
				message: error.message,
			});
		}
	}

	async updateSupplier(req, res) {
		try {
			const supplier = await query(
				`SELECT * FROM suppliers WHERE id = ${req.params.id}`
			);
			if (supplier.length == 0) {
				return res.status(404).json({
					message: `Supplier with id = ${req.params.id} is not found`,
				});
			}
			const update = await query(
				`UPDATE suppliers SET name="${req.body.name}" WHERE id = ${req.params.id};`
			);
			const result = await query(
				`SELECT * FROM suppliers WHERE id = ${req.params.id}`
			);
			res.status(201).json({
				data: result,
			});
		} catch (error) {
			res.status(500).json({
				message: error.message,
			});
		}
	}

	async deleteSupplier(req, res) {
		try {
			const deletedData = await query(
				`DELETE FROM suppliers WHERE id=${req.params.id}`
			);

			if (deletedData.affectedRows === 0) {
				return res.status(404).json({
					message: "Supplier not found",
				});
			}

			res.status(200).json({
				message: `Supplier with id ${req.params.id} has been deleted`,
			});
		} catch (error) {
			res.status(500).json({
				message: error.message,
			});
		}
	}
}

module.exports = new Suppliers();
