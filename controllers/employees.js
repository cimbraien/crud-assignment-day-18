const { query } = require("../models");

class Employee {
  async createEmployee(req, res) {
    try {
      const data = await query(
        `INSERT INTO employees(name) VALUES ("${req.body.name}")`
      );
      const newEmployee = await query(
        `SELECT * FROM employees WHERE id = ${data.insertId}`
      );
      res.status(201).json({
        data: newEmployee,
      });
    } catch (error) {
      res.status(500).json({
        message: error.message,
      });
    }
  }

  async readAllEmployees(req, res) {
    try {
      const data = await query(`SELECT * FROM employees`);
      res.status(200).json({
        data: data,
      });
    } catch (error) {
      res.status(500).json({
        message: error.message,
      });
    }
  }

  async readEmployeeById(req, res) {
    try {
      const data = await query(
        `SELECT * FROM employees WHERE id = ${req.params.id}`
      );
      if (data.length == 0) {
        return res.status(404).json({
          message: `The employee by id: ${req.params.id} is not found`,
        });
      }
      res.status(200).json({
        data: data,
      });
    } catch (error) {
      res.status(500).json({
        message: error.message,
      });
    }
  }

  async updateEmployee(req, res) {
    try {
      const data = await query(
        `SELECT * FROM employees WHERE id = ${req.params.id}`
      );
      if (data.length == 0) {
        return res.status(404).json({
          message: `The employee by id: ${req.params.id} is not found`,
        });
      }
      await query(
        `UPDATE employees SET name="${req.body.name}" WHERE id = ${req.params.id};`
      );
      const updatedEmployee = await query(
        `SELECT * FROM employees WHERE id = ${req.params.id}`
      );
      res.status(201).json({
        data: updatedEmployee,
      });
    } catch (error) {
      res.status(500).json({
        message: error.message,
      });
    }
  }

  async deleteEmployee(req, res) {
    try {
      const deletedEmployee = await query(
        `DELETE FROM employees WHERE id=${req.params.id}`
      );

      if (deletedEmployee.affectedRows === 0) {
        return res.status(404).json({
          message: "The employee is not found",
        });
      }

      res.status(200).json({
        message: `The employee by id: ${req.params.id} has been deleted`,
      });
    } catch (error) {
      res.status(500).json({
        message: error.message,
      });
    }
  }
}

module.exports = new Employee();
