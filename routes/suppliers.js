const express = require("express"); // import express

const {
	getAllSuppliers,
	getSupplierById,
	createSupplier,
	updateSupplier,
	deleteSupplier,
} = require("../controllers/suppliers");

const router = express.Router(); // make express router

router.get("/", getAllSuppliers);

router.get("/:id", getSupplierById);

router.post("/", createSupplier);

router.put("/:id", updateSupplier);

router.delete("/:id", deleteSupplier);

module.exports = router;
