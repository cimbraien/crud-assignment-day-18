const express = require("express");
const router = express.Router();

const dataController = require("../controllers/customers");

router.get("/", dataController.getAllCustomers);
router.get("/:id", dataController.getCustomerById);
router.post("/", dataController.createCustomer);
router.put("/:id", dataController.updateCustomer);
router.delete("/:id", dataController.deleteCustomer);

module.exports = router;
