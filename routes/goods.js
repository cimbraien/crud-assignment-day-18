const express = require("express");
const { getAllGoods, getOneGoods, addGoods, editGoods, deleteGood } = require("../controllers/goods");
const router = express.Router();

router.get("/", getAllGoods);
router.get("/:id", getOneGoods);

router.post("/", addGoods);

router.put("/:id", editGoods);

router.delete("/:id", deleteGood);

module.exports = router;
