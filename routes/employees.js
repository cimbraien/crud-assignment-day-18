// Call Express
const express = require("express");

// Import controllers
const {
  createEmployee,
  readAllEmployees,
  readEmployeeById,
  updateEmployee,
  deleteEmployee,
} = require("../controllers/employees");

// Define express router
const router = express.Router();

// Router function
router.get("/", readAllEmployees);

router.get("/:id", readEmployeeById);

router.post("/", createEmployee);

router.put("/:id", updateEmployee);

router.delete("/:id", deleteEmployee);

module.exports = router;
