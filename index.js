const express = require("express"); // Import express

const app = express(); // Make express app

/* Import routes */
const transactions = require("./routes/transactions");
const suppliers = require("./routes/suppliers");
const goods = require("./routes/goods");
const customers = require("./routes/customers");
const employees = require("./routes/employees");

/* Enable req.body */
app.use(express.json()); // Enable req.body JSON
// Enable url-encoded
app.use(
	express.urlencoded({
		extended: true,
	})
);

/* Use routes */
app.use("/transactions", transactions);
app.use("/suppliers", suppliers);
app.use("/goods", goods);
app.use("/customers", customers);
app.use("/employees", employees);
/* Running server */
app.listen(3000, () => console.log(`Server running on port 3000!`));
